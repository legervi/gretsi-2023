import numpy as np
from numpy.linalg import inv

class Data:
    """Data

    T : number of tasks
    m : number of classes
    p : dimension of data
    NL : (T,m)-array with the number of labeled data
    NU : (T,m)-array with the number of unlabeled data
    labeled : (T,m)-list with (p,NL[t,j])-arrays of data
    unlabeled : (T,m)-list with (p,NU[t,j])-arrays of data
    """
    def __init__(self,p,NL,NU):
        self.NL = np.array(NL)
        self.NU = np.array(NU)
        if self.NL.shape!=self.NU.shape:
            print("Formats incompatibles")
        self.T = len(NL)
        self.m = len(NL[0])
        self.p = p
        self.labeled = [[0 for j in range(self.m)] for t in range(self.T)]
        for t in range(self.T):
            for j in range(self.m):
                self.labeled[t][j] = np.zeros((self.p,self.NL[t,j]))
        self.unlabeled = [[0 for j in range(self.m)] for t in range(self.T)]
        for t in range(self.T):
            for j in range(self.m):
                self.unlabeled[t][j] = np.zeros((self.p,self.NU[t,j]))

    def create_gaussian(self,MU):
        """Generate gaussian isotropic data for a given set of means MU"""
        # for t in range(self.T):
        #     for j in range(self.m):
        #         self.labeled[t][j] = np.random.multivariate_normal(MU[t,j],np.eye(self.p),size=self.NL[t,j]).T
        #         self.unlabeled[t][j] = np.random.multivariate_normal(MU[t,j],np.eye(self.p),size=self.NU[t,j]).T
        temp = np.random.multivariate_normal(np.zeros(self.p),np.eye(self.p),size=np.sum(self.NL)+np.sum(self.NU))
        temp_l = temp[0:np.sum(self.NL)]
        temp_u = temp[np.sum(self.NL)::]
        il,iu = 0,0
        for t in range(self.T):
            for j in range(self.m):
                self.labeled[t][j] = temp_l[il:il+self.NL[t,j]].T + np.reshape(MU[t,j],(self.p,1))
                self.unlabeled[t][j] = temp_u[iu:iu+self.NU[t,j]].T + np.reshape(MU[t,j],(self.p,1))
                il = il+self.NL[t,j]
                iu = iu+self.NU[t,j]

    def center(self):
        """Center both labeled and unlabeled data task-wise"""
        for t in range(self.T):
            nlt = np.sum(self.NL[t])
            nut = np.sum(self.NU[t])
            Vt = np.zeros(self.p)
            for j in range(self.m):
                Vt += np.sum(self.labeled[t][j],axis=1)
                Vt += np.sum(self.unlabeled[t][j],axis=1)
            Vt = np.reshape(Vt,(self.p,1))/(nlt+nut)
            for j in range(self.m):
                self.labeled[t][j] -= Vt@np.ones((1,self.NL[t,j]))
                self.unlabeled[t][j] -= Vt@np.ones((1,self.NU[t,j]))

    def Z(self):
        """Return the data as Z=(Zl,Zu) format"""
        Zl = np.zeros((self.T*self.p,np.sum(self.NL)))
        Zu = np.zeros((self.T*self.p,np.sum(self.NU)))
        for t in range(self.T):
            Zl[t*self.p:(t+1)*self.p,np.sum(self.NL[0:t]):np.sum(self.NL[0:t+1])] = np.hstack(self.labeled[t])
            Zu[t*self.p:(t+1)*self.p,np.sum(self.NU[0:t]):np.sum(self.NU[0:t+1])] = np.hstack(self.unlabeled[t])
        return Zl,Zu

    def Mcal(self,method,MU=0):
        """Return the value of Mcal

        method='true' : Use ground truth means MU
        method='estimate' : Use the estimator with splitted samples
        """
        Mcal = np.zeros((self.m*self.T,self.m*self.T))
        if method=='true':
            MUc = MU.copy()
            for t in range(self.T):
                vt = np.zeros(self.p)
                for j in range(self.m):
                    vt += (self.NL[t,j]+self.NU[t,j])*MU[t,j]
                vt /= np.sum(self.NL[t]+self.NU[t])
                for j in range(self.m):
                    MUc[t,j] -= vt
            M = np.reshape(MUc,(self.m*self.T,self.p)).T
            Mcal = M.T@M
        elif method=='estimate':
            for t1 in range(self.T):
                for j1 in range(self.m):
                    for t2 in range(self.T):
                        for j2 in range(self.m):
                            Mcal[self.m*t1+j1,self.m*t2+j2] = np.dot(np.mean(self.Xl(t1,j1),axis=1),np.mean(self.Xl(t2,j2),axis=1))
            for t in range(self.T):
                for j in range(self.m):
                    Xlt = self.Xl(t,j)
                    n = len(Xlt[0])
                    Mcal[self.m*t+j,self.m*t+j] = np.dot(np.mean(Xlt[:,0:n//2],axis=1),np.mean(Xlt[:,n//2::],axis=1))
        else:
            print("Invalid value of method")
        return Mcal

    def scores(self, hp, y, tt):
        """Computes the score vector for target task tt"""
        A = np.kron(hp.lbd_tilde(),np.eye(self.p))
        Zl,Zu = self.Z()
        Q_prime = inv(np.eye(np.sum(self.NU))-(Zu.T@A@Zu)/(self.T*self.p))
        Yl = np.zeros(np.sum(self.NL))
        for t in range(self.T):
            yl1 = y[2*t]*np.ones(self.NL[t,0])
            yl2 = y[2*t+1]*np.ones(self.NL[t,1])
            Yl[np.sum(self.NL[0:t]):np.sum(self.NL[0:t+1])] = np.concatenate((yl1,yl2))
        Fu = (Q_prime@Zu.T@A@Zl@Yl)/(self.T*self.p)
        return Fu[np.sum(self.NU[0:tt]):np.sum(self.NU[0:tt+1])]

    def error_emp(self, hp, y, tt, threshold):
        """Computes prediction error with a given threshold in the case unlabeled data is ordered"""
        return 1-np.mean((self.scores(hp, y, tt)>threshold)==np.concatenate([np.zeros(self.NU[tt][0]),np.ones(self.NU[tt][1])]))

class DataDegree(Data):
    """
    degrees : (NL.shape)-array with confidence degree that data belongs to one class or the other
    """
    def __init__(self,p,NL,NU):
        super().__init__(p,NL,NU)
        # self.degrees = [[[0 for j2 in range(self.m)] for j1 in range(self.m)] for t in range(self.T)]
        self.degrees = [[0 for j in range(self.m)]for t in range(self.T)]
        for t in range(self.T):
            for j in range(self.m):
                self.degrees[t][j] = np.zeros(np.sum(self.NL[t]))
            # for j1 in range(self.m):
            #     for j2 in range(self.m):
            #         self.degrees[t][j1][j2] = np.zeros(self.NL[t,j1])
        self.N = self.NL + self.NU

    def Z(self):
        """Return the data as Z format"""
        Z = np.zeros((self.T*self.p,np.sum(self.NL)+np.sum(self.NU)))
        for t in range(self.T):
            Z[t*self.p:(t+1)*self.p,np.sum(self.N[0:t]):np.sum(self.N[0:t])+np.sum(self.NL[t])] = np.hstack(self.labeled[t])
            Z[t*self.p:(t+1)*self.p,np.sum(self.N[0:t])+np.sum(self.NL[t]):np.sum(self.N[0:t+1])] = np.hstack(self.unlabeled[t])
        return Z

    def scores(self, hp, y, tt):
        """Computes the score vector for target task tt"""
        A = np.kron(hp.lbd_tilde(),np.eye(self.p))
        Z = self.Z()
        Q_prime = inv(np.eye(np.sum(self.N))-(Z.T@A@Z)/(self.T*self.p))
        Y = np.zeros(np.sum(self.N))
        for t in range(self.T):
            Y[np.sum(self.N[0:t]):np.sum(self.N[0:t])+np.sum(self.NL[t])] = y[2*t]*self.degrees[t][0] + y[2*t+1]*self.degrees[t][1]
            Y[np.sum(self.N[0:t])+np.sum(self.NL[t]):np.sum(self.N[0:t+1])] = np.zeros(np.sum(self.NU[t]))
        F = Q_prime@Y
        return F[np.sum(self.N[0:tt])+np.sum(self.NL[tt]):np.sum(self.N[0:tt+1])]