import numpy as np
from scipy.optimize import fsolve
from numpy.linalg import inv
from scipy.optimize import minimize_scalar
from math import isnan
from small_dim import *

class HyperParameters:
    """Hyperparameters, i.e. Lambda and alpha"""
    def __init__(self,T):
        self.lbd = np.ones((T,T))
        self.alpha=1
        self.alpha0=1

    def lbd_tilde(self):
        return self.lbd/self.alpha

    def compute_lambda(self,data,method,MU=0):
        """Set the value of Mcal associated to the data

        method='true' : Use ground truth means MU
        method='estimate' : Use the estimated mean
        """
        if method=='true':
            for t1 in range(data.T):
                for t2 in range(data.T):
                    self.lbd[t1,t2] = np.abs(np.dot(MU[t1,0]-MU[t1,1],MU[t2,0]-MU[t2,1]))/(np.linalg.norm(MU[t1,0]-MU[t1,1],ord=2)*(np.linalg.norm(MU[t2,0]-MU[t2,1],ord=2)))
        elif method=='estimate':
            for t1 in range(data.T):
                m_0_0 = np.mean(data.Xl(t1,0)[:,0:n//2],axis=1)-np.mean(data.Xl(t1,1)[:,0:n//2],axis=1)
                m_0_1 = np.mean(data.Xl(t1,0)[:,n//2::],axis=1)-np.mean(data.Xl(t1,1)[:,n//2::],axis=1)
                for t2 in range(data.T):
                    m_1_0 = np.mean(data.Xl(t2,0)[:,0:n//2],axis=1)-np.mean(data.Xl(t2,1)[:,0:n//2],axis=1)
                    m_1_1 = np.mean(data.Xl(t2,0)[:,n//2::],axis=1)-np.mean(data.Xl(t2,1)[:,n//2::],axis=1)
                    hp.lbd[t1,t2] = np.abs(np.dot((m_0_0+m_0_1)/2,(m_1_0+m_1_1)/2))/(np.sqrt(np.dot(m_0_0,m_0_1))*np.sqrt(np.dot(m_1_0,m_1_1)))
            for t in range(data.T):
                self.lbd[t,t] = 1
        elif method=='ord1':
            for t1 in range(data.T):
                for t2 in range(data.T):
                    self.lbd[t1,t2] = np.sqrt(np.abs(np.dot(MU[t1,0]-MU[t1,1],MU[t2,0]-MU[t2,1]))/(np.linalg.norm(MU[t1,0]-MU[t1,1],ord=2)*(np.linalg.norm(MU[t2,0]-MU[t2,1],ord=2))))
        elif method=='ord2':
            for t1 in range(data.T):
                for t2 in range(data.T):
                    self.lbd[t1,t2] = np.abs(np.dot(MU[t1,0]-MU[t1,1],MU[t2,0]-MU[t2,1]))/(np.linalg.norm(MU[t1,0]-MU[t1,1],ord=2)*(np.linalg.norm(MU[t2,0]-MU[t2,1],ord=2)))
        elif method=='ord4':
            for t1 in range(data.T):
                for t2 in range(data.T):
                    self.lbd[t1,t2] = (np.abs(np.dot(MU[t1,0]-MU[t1,1],MU[t2,0]-MU[t2,1]))/(np.linalg.norm(MU[t1,0]-MU[t1,1],ord=2)*(np.linalg.norm(MU[t2,0]-MU[t2,1],ord=2))))**2
        else:
            print("Invalid value of method")

    def compute_alpha0(self,data):
        """Set the value of self.alpha0 to the minimum ensuring a good conditionment of delta"""
        T = data.T
        if type(data).__name__=='DataDegree':
            c = data.p/np.sum(data.N)
            rho = data.N/np.sum(data.N)
        elif type(data).__name__=='Data':
            c = data.p/np.sum(data.NU)
            rho = data.NU/np.sum(data.NU)
        inv_D_rho = np.diag(1/np.sum(rho,axis=1))
        lbd = self.lbd
        Y0 = []
        for t in range(T):
            def gt(x):
                return (lbd@inv(x*inv_D_rho-lbd)@inv_D_rho@inv(x*inv_D_rho-lbd)@lbd)[t,t]-1/c
            Y0.append(fsolve(gt,np.max(np.real(np.linalg.eigvals(lbd@np.diag(np.sum(rho,axis=1)))))+np.sqrt(c))[0])
        y0 = np.max(Y0)
        self.alpha0 = np.max(np.diag(lbd+lbd@inv(y0*inv_D_rho-lbd)@lbd)/T+y0/(T*c))

    def optim_alpha(self, data, Mcal, tt, K_bar=0, tol=1e-3):
        """Set the value of self.alpha that minimizes "error_opti"

        tol : maximum relative error acceptable
        """
        if type(data).__name__=='DataDegree':
            def sd(data, self, Mcal, tt, K_bar):
                return small_dim_deg(data, self, Mcal, tt, K_bar)
        elif type(data).__name__=='Data':
            def sd(data, self, Mcal, tt, K_bar):
                return small_dim(data, self, Mcal, tt)

        def funk(alpha):
            self.alpha = alpha
            a1, a2, B = sd(data, self, Mcal, tt, K_bar)
            e = error_opti(a1,a2,B)
            if isnan(e):
                return 0.5
            else:
                return e

        x1, x2, x3 = 1.5*self.alpha0, 2*self.alpha0, 3*self.alpha0
        e1, e2, e3 = funk(x1), funk(x2), funk(x3)
        while e2>e1 or e2>e3:
            if e2>e1:
                x1, x2, x3 = ((x1/self.alpha0+1)/2)*self.alpha0, x1, x2
                e1, e2, e3 = funk(x1), e1, e2
            elif e2>e3:
                x1, x2, x3 = x2, x3, (2*x3/self.alpha0-1)*self.alpha0
                e1, e2, e3 = e2, e3, funk(x3)
        self.alpha = minimize_scalar(funk, bracket=[x1,x2,x3], method='golden', tol=tol).x