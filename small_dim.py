import numpy as np
from scipy.optimize import fsolve
from numpy.linalg import inv
import scipy.stats as stat

def error_th(a1, a2, B, y, tt, threshold):
    """Computes expected error with a given threshold and given labels"""
    return (stat.norm.sf(-np.dot(a1,y)/np.sqrt(y@B@y)) + stat.norm.sf(np.dot(a2,y)/np.sqrt(y@B@y)))/2

def error_opti(a1, a2, B):
    """Computes expected error with optimal labels"""
    return stat.norm.sf(0.5*np.sqrt((a2-a1)@inv(B)@(a2-a1)))

def labels_opti(a1, a2, B):
    """Computes optimal label vector"""
    return inv(B)@(a2-a1)

def small_dim(data, hp, Mcal, tt):
    """Computes small dimensional quantities a1,a2,B associated to target task tt"""
    Lambda_tilde = hp.lbd_tilde()
    T = data.T
    cl, cu = data.p/np.sum(data.NL), data.p/np.sum(data.NU)
    rho_l, rho_u = data.NL/np.sum(data.NL), data.NU/np.sum(data.NU)
    rho_u_bar = np.sum(rho_u,axis=1)
    v_rho_u = rho_u.flatten()

    def f(x):
        return x - np.diag(Lambda_tilde+Lambda_tilde@inv(np.diag((T*cu*(1-x))/rho_u_bar)-Lambda_tilde)@Lambda_tilde)/T
    delta = fsolve(f,np.diag(Lambda_tilde+Lambda_tilde@inv(np.diag(T*cu/rho_u_bar)-Lambda_tilde)@Lambda_tilde)/T)
    delta_tilde = np.kron(1/(T*cu*(1-delta)),[1,1])*v_rho_u
    delta_bar = np.sum(delta_tilde.reshape((T,2)),axis=1)
    Acal = Lambda_tilde+Lambda_tilde@inv(np.diag(1/delta_bar)-Lambda_tilde)@Lambda_tilde

    D_rho_l = np.diag(rho_l.flatten())
    D_tilde = np.diag(delta_tilde)

    Gamma = inv(np.eye(2*T)-np.diag(delta_tilde)@(np.kron(Acal,np.ones((2,2)))*Mcal))
    S_bar = np.zeros((T,T))
    for t1 in range(T):
        for t2 in range(T):
            S_bar[t1,t2] = Acal[t1,t2]**2/(T**2*cu*(1-delta[t2])**2)
    if np.sign(np.linalg.det(np.eye(T)-S_bar))<0:
        S = -S_bar@inv(np.eye(T)-np.diag(rho_u_bar)@S_bar)
        if np.linalg.det(np.eye(T)-S_bar)<-1e-1:
            print("Bizarre...{}".format(np.linalg.det(np.eye(T)-S_bar)))
            print(S)
    else:
        S = S_bar@inv(np.eye(T)-np.diag(rho_u_bar)@S_bar)

    d = rho_u_bar*S[tt]
    d[tt] += 1
    Vcal = Acal@(np.diag(d))@Acal/(T*(1-delta[tt]))**2
    H1 = Gamma.T@(np.kron(Vcal,np.ones((2,2)))*Mcal)@Gamma
    H2 = (Gamma.T-np.eye(2*T))@np.diag(v_rho_u*np.repeat(S[tt],2)/delta_tilde**2)@(Gamma-np.eye(2*T))/(T*(1-delta[tt]))**2
    B = D_rho_l@(H1+H2)@D_rho_l/cl**2 + cu/cl*D_rho_l*np.diag(np.repeat(S[:,tt],2))
    a = cu/cl*D_rho_l@(Gamma.T-np.eye(2*T))@inv(np.diag(v_rho_u))
    e1, e2 = np.zeros(2*T), np.zeros(2*T)
    e1[2*tt], e2[2*tt+1] = 1, 1
    a1, a2 = np.dot(a,e1), np.dot(a,e2)
    return a1, a2, B

def small_dim_deg(data, hp, Mcal, tt, K_bar):
    """Computes small dimensional quantities a1,a2,B associated to target task tt"""
    Lambda_tilde = hp.lbd_tilde()
    T = data.T
    c = data.p/np.sum(data.N)
    # cl, cu = data.p/np.sum(data.NL), data.p/np.sum(data.NU)
    rho = data.N/np.sum(data.N)
    # rho_l, rho_u = data.NL/np.sum(data.NL), data.NU/np.sum(data.NU)
    rho_bar = np.sum(rho,axis=1)
    # rho_u_bar = np.sum(rho_u,axis=1)
    v_rho = rho.flatten()
    # v_rho_u = rho_u.flatten()

    def f(x):
        return x - np.diag(Lambda_tilde+Lambda_tilde@inv(np.diag((T*c*(1-x))/rho_bar)-Lambda_tilde)@Lambda_tilde)/T
    delta = fsolve(f,np.diag(Lambda_tilde+Lambda_tilde@inv(np.diag(T*c/rho_bar)-Lambda_tilde)@Lambda_tilde)/T)
    delta_tilde = np.kron(1/(T*c*(1-delta)),[1,1])*v_rho
    delta_bar = np.sum(delta_tilde.reshape((T,2)),axis=1)
    Acal = Lambda_tilde+Lambda_tilde@inv(np.diag(1/delta_bar)-Lambda_tilde)@Lambda_tilde

    # D_rho_l = np.diag(rho_l.flatten())
    D_tilde = np.diag(delta_tilde)

    Gamma = inv(np.eye(2*T)-np.diag(delta_tilde)@(np.kron(Acal,np.ones((2,2)))*Mcal))
    S_bar = np.zeros((T,T))
    for t1 in range(T):
        for t2 in range(T):
            S_bar[t1,t2] = Acal[t1,t2]**2/(T**2*c*(1-delta[t2])**2)
    if np.sign(np.linalg.det(np.eye(T)-S_bar))<0:
        S = -S_bar@inv(np.eye(T)-np.diag(rho_bar)@S_bar)
        if np.linalg.det(np.eye(T)-S_bar)<-1e-3:
            print("Bizarre...{}".format(np.linalg.det(np.eye(T)-S_bar)))
    else:
        S = S_bar@inv(np.eye(T)-np.diag(rho_bar)@S_bar)

    d = rho_bar*S[tt]
    d[tt] += 1
    Vcal = Acal@(np.diag(d))@Acal
    H1 = Gamma.T@(np.kron(Vcal,np.ones((2,2)))*Mcal)@Gamma
    H2 = (Gamma.T-np.eye(2*T))@np.diag(v_rho*np.repeat(S[tt],2))@(Gamma-np.eye(2*T))

    K_tilde_T = np.zeros((2*T,2*T))
    for t in range(T):
        for j1 in range(2):
            for j2 in range(2):
                K_tilde_T[2*t+j1,2*t+j2] = np.sum(data.NL[t])/np.sum(data.N[t])*rho_bar[t]*S[tt,t]*np.mean(data.degrees[t][j1]*data.degrees[t][j2])

    B = (K_bar.T@D_tilde@H1@D_tilde@K_bar + K_bar.T@H2@K_bar + K_tilde_T)/(1-delta[tt])**2
    a = K_bar.T@(Gamma-np.eye(2*T))
    e1, e2 = np.zeros(2*T), np.zeros(2*T)
    e1[2*tt], e2[2*tt+1] = 1, 1
    a1, a2 = np.dot(a,e1)/(1-delta[tt]), np.dot(a,e2)/(1-delta[tt])
    return a1, a2, B