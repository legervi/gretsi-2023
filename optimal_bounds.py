import numpy as np
import quadpy as qp
from scipy.optimize import fsolve

def psi(x,t):
    u = np.exp(-2*(np.sqrt(2*x)*t+x))
    return (1-u)/(1+u)*(1+t/(np.sqrt(2*x)))

def h_prime(x):
    scheme = qp.e1r2.gauss_hermite(21)
    return 1/np.sqrt(np.pi)*scheme.integrate(lambda t: psi(x,t))[0]

def phi(x):
    return -1 + 2*h_prime(x)

def solution(data, M):
    alphas = (np.sum(data.NL,axis=1)+np.sum(data.NU,axis=1))/data.p
    etas = np.sum(data.NL,axis=1)/(np.sum(data.NL,axis=1)+np.sum(data.NU,axis=1))
    T = data.T
    def eqs(q):
        qu = q[0:T]
        qv = q[T::]
        D = np.diag(alphas*qv)
        return np.concatenate((qu-np.diag(M-M@np.linalg.inv(np.eye(T)+D@M)),[qv[t]-etas[t]-(1-etas[t])*phi(qu[t]) for t in range(T)]))
    return fsolve(eqs, np.ones(2*T))